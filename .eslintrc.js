module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": "airbnb-base",
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "settings": {
        "import/resolver": "webpack"
    },
    "rules": {
        "linebreak-style": 0,
        "max-len": [ "error", {
            "code": 120
        }],
        "arrow-parens": ["error", "as-needed"],
        "class-methods-use-this": "off",
        "no-param-reassign": [2, { "props": false }],
        "object-curly-newline": ["error", {  "ObjectPattern": { "multiline": true, "minProperties": 8 }}]
    }
};
