/**
 * Pure js json file upload
 * @param files
 * @returns {Promise<any>}
 */
export default function uploadSonData(files) {
  return new Promise(resolve => {
    if (files.length === 0) {
      throw new Error('No file selected');
    }

    // first file selected by user
    const file = files[0];

    // files types allowed
    const allowedTypes = ['application/json'];
    if (allowedTypes.indexOf(file.type) === -1) {
      throw new Error('File has incorrect type');
    }

    // Max 2 MB allowed
    const maxSizeAllowed = 2 * 1024 * 1024;
    if (file.size > maxSizeAllowed) {
      throw new Error('File exceeded size 2MB');
    }

    // file validation is successful
    // we will now read the file
    const reader = new FileReader();

    reader.addEventListener('load', ({ target }) => {
      try {
        const jsonStr = JSON.parse(target.result);
        resolve(jsonStr);
      } catch (e) {
        throw new Error('File corrupted');
      }
    });

    reader.readAsText(file);
  });
}
