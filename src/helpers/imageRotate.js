import config from 'config';

const { antImageWidth, antImageHeight } = config;


export default function rotate(img, angle) {
  const rotationCanvas = document.createElement('canvas');
  const ctx = rotationCanvas.getContext('2d');

  rotationCanvas.width = antImageWidth;
  rotationCanvas.height = antImageHeight;

  ctx.translate(antImageWidth / 2, antImageHeight / 2);
  ctx.rotate(angle);
  ctx.drawImage(img, -antImageWidth / 2, -antImageHeight / 2, antImageWidth, antImageHeight);

  return rotationCanvas;
}
