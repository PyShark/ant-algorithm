
/*
 * This function help two convert rgb to hex.
 */
export default function rgbToHex(r, g, b) {
  if (r > 255 || g > 255 || b > 255) { throw new Error('Invalid color component'); }

  // eslint-disable-next-line no-bitwise
  return ((r << 16) | (g << 8) | b).toString(16);
}
