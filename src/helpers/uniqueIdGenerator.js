
/*
 * Using es6 generator for generating unique id.
 */
let i = 1;
export default (function* generateId() {
  for (i; ; i += 1) { yield i; }
}());

export function resetGenerator() {
  i = 1;
}
