// Some helpers for helping drawing

/**
 * Return distance and angle between two points
 * @param x1
 * @param y1
 * @param x2
 * @param y2
 * @returns {{distance: number, angle: number}}
 */
export function getDistanceBetween(x1, y1, x2, y2) {
  const x = x2 - x1;
  const y = y2 - y1;

  return Math.hypot(x, y);
}

export function calculateVectorMagnitude(point, target, velocity) {
  const tx = target.posX - point.posX;
  const ty = target.posY - point.posY;

  const dist = Math.sqrt(tx * tx + ty * ty);
  return { x: ((tx / dist) * velocity) || 0, y: ((ty / dist) * velocity) || 0, dist };
}
