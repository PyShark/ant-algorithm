/**
 *
 * @param probabilities {number[]} - number of probability percents
 * @returns {number} - index from probabilities
 */
export default function randomDistribute(probabilities) {
  const arDistribute = [];
  let i;
  let sum = 0;

  for (i = 0; i < probabilities.length - 1; i += 1) {
    sum += (probabilities[i] / 100.0);
    arDistribute[i] = sum;
  }

  const r = Math.random(); // returns [0,1]

  for (i = 0; i < arDistribute.length && r >= arDistribute[i]; i += 1) ;

  return i;
}
