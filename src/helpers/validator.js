/**
 * Simple validator with rules
 * @param el
 * @param rules {...function}
 * @returns {*}
 */
export default function validate(el, ...rules) {
  if (!(el && rules.every(r => r(el)))) { throw new Error('Validation failed'); }

  return el;
}
