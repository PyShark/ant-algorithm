import config from 'config';
import idGenerator from 'helpers/uniqueIdGenerator';
import EventEmitter from 'helpers/eventEmitter';

const gridElementEmitter = new EventEmitter();

class GridElement {
  /**
   * @param context{string} - context name
   * @param originLayerId
   * @param originState
   */
  constructor(context, originLayerId, originState) {
    this.id = idGenerator.next().value;

    this.state = originState;

    this.originState = originState;
    this.originLayerId = originLayerId;

    this.contextName = context;
    this.context = document.createElementNS(config.SVGNamespace, context);
  }

  /**
   * Set click event to grid element
   * @param handler
   */
  addClickEvent(handler) {
    this.context.addEventListener('click', e => handler(e));
  }

  emitElementCreated() {
    gridElementEmitter.emit(`${this.contextName}_created`, this);
  }

  emitElementDeleted() {
    gridElementEmitter.emit(`${this.contextName}_deleted`, this);
  }

  static listenElementCreated(entityName, handler) {
    gridElementEmitter.on(`${entityName}_created`, handler);
  }

  static listenElementDeleted(entityName, handler) {
    gridElementEmitter.on(`${entityName}_deleted`, handler);
  }

  /**
   * Override this function which sets additional data to context file ctx updates
   */
  updateContextFunc() {}

  /**
   * Update state of grid elements.
   * @param state{{id, class}}
   * @param options{{
   * forceUpdateClass?: boolean, forceUpdateState?: boolean,
   * useToggle?: boolean, resetClass?: string, removeClass?: string }}
   */
  updateCtxFromState(state = this.state, options = {}) {
    const opt = {
      forceUpdateClass: true,
      forceUpdateState: true,
      useToggle: false,
      resetClass: false,
      removeClass: false,
      ...options,
    };

    if (opt.removeClass && !opt.resetClass) {
      this.context.classList.remove(opt.removeClass);
    }

    if (opt.forceUpdateClass) {
      if (opt.useToggle) this.context.classList.toggle(this.state.class);
      else this.context.classList.add(state.class);
    }

    if (opt.forceUpdateState && state !== this.state) {
      this.state = state;

      if (opt.resetClass) {
        this.context.classList.remove(...this.context.classList);
        this.context.classList.add(opt.resetClass);
        this.context.classList.add(state.class);
      }
    }

    // here update context variables from current state object
    this.updateContextFunc(state);
  }

  /**
   * Return data serialize or deserialize
   * @returns {{}}
   */
  getSerializeElement() { return this; }

  static deserializeElement(jsonData, err) { // yeah, should be hide in interfaces, but who cares.
    if (!jsonData) { throw err; }
    return jsonData;
  }

  /**
   * Remove item from context
   */
  removeItem() {
    this.emitElementDeleted();
  }
}

export default GridElement;
