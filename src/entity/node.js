
/*
 * Node entity which was our nodes in Graph.
 */
import config from 'config';

import GridElement from 'entity/base/GridElement';

import validate from 'helpers/validator';

export const states = {
  primary: {
    id: 1,
    class: 'grid-circle',
    radius: config.nodeSizeSmall,
  },
  click: {
    id: 2,
    class: 'circle-click',
    radius: config.nodeSizeExtraBig,
  },
  startPos: {
    id: 3,
    class: 'start-pos',
    radius: config.nodeSizeBig,
  },
  endPos: {
    id: 4,
    class: 'end-pos',
    radius: config.nodeSizeBig,
  },
  path: {
    id: 5,
    class: 'trail-point',
    radius: config.nodeSizeBig,
  },
};

const STATES_MAP = Object.keys(states).reduce((acc, cur) => { acc[states[cur].id] = states[cur]; return acc; }, {});

class Node extends GridElement {
  constructor(x, y, originLayerId, originState = states.primary) {
    super('circle', originLayerId, originState);

    this.radius = config.nodeSizeSmall;
    this.diameter = this.radius * 2;

    this.posX = this.setPosition(x);
    this.posY = this.setPosition(y);

    this.linkedDirections = [];

    this.emitElementCreated();
  }

  updateContextFunc(state) {
    this.context.setAttributeNS(null, 'r', `${state.radius}`);
  }

  setPosition(posValue) {
    return this.radius + this.diameter * posValue + config.indentSize * posValue + config.nodeSizeSmall;
  }

  isMainPoint() {
    return [this.state.id, this.originState.id].some(e => [states.startPos.id, states.endPos.id].includes(e));
  }

  changeOriginState(state) { // note: state is also bind to layer by id
    this.originState = state;
    this.originLayerId = state.id;
  }

  isLinkedTo(node) {
    return this.linkedDirections.some(n => n.target.id === node.id);
  }

  /**
   * Link to other node
   * @param direction {Direction}
   */
  linkNode(direction) {
    this.linkedDirections.push(direction);
  }

  /**
   * Return next random Direction
   * @returns {Direction}
   */
  getRandomDirection() {
    return this.linkedDirections[Math.floor(Math.random() * this.linkedDirections.length)];
  }

  getSerializeElement() {
    return {
      id: this.id,
      x: this.posX,
      y: this.posY,
      st: this.originState.id,
      // save only shorthand id of nodes for prevent circular serialization
      ln: this.linkedDirections.map(e => e.target.id),
    };
  }

  static deserializeElement(el) {
    return {
      id: validate(el.id, Number.isInteger),
      posX: validate(el.x, Number.isInteger),
      posY: validate(el.y, Number.isInteger),
      originState: STATES_MAP[validate(el.st, e => STATES_MAP[e])],
      ln: validate(el.ln, Array.isArray, e => e.every(n => Number.isInteger(n))),
    };
  }
}

export default Node;
