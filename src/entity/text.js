import GridElement from 'entity/base/GridElement';

export const states = {
  primary: {
    id: 7,
    class: 'grid-text',
  },
  lineText: {
    id: 8,
    class: 'grid-line-text',
  },
};

class Text extends GridElement {
  /**
   * Create text on some position
   * @param x
   * @param y
   * @param originElement {GridElement}
   */
  constructor(x, y, originElement = null) {
    super('text', null, states.lineText);

    this.posX = x;
    this.posY = y;

    this.originElement = originElement;

    this.emitElementCreated();
  }
}

export default Text;
