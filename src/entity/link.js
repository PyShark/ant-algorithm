
import GridElement from 'entity/base/GridElement';
import Direction from 'entity/direction';

export const states = {
  primary: {
    id: 6,
    class: 'grid-line',
  },
};

class Link extends GridElement {
  /**
   * Create new link between two nodes
   * @param nodeA{Node}
   * @param nodeB{Node}
   * @param originLayerId{number}
   * @param originState{states}
   */
  constructor(nodeA, nodeB, originLayerId, originState = states.primary) {
    super('line', originLayerId, originState);

    this.startPos = { id: nodeA.id, x: nodeA.posX, y: nodeA.posY };
    this.endPos = { id: nodeB.id, x: nodeB.posX, y: nodeB.posY };

    this.distance = Math.round(Math.hypot(
      nodeA.posX - nodeB.posX,
      nodeA.posY - nodeB.posY,
    ));

    const ADirection = new Direction(nodeA, nodeB, this);
    const BDirection = new Direction(nodeB, nodeA, this);

    ADirection.backward = BDirection;
    BDirection.backward = ADirection;

    nodeA.linkNode(ADirection);
    nodeB.linkNode(BDirection);

    this.emitElementCreated();
  }

  /**
   * Attach to link text
   * @param text{Text}
   */
  setLinkText(text) {
    this.text = text;
  }
}

export default Link;
