import Direction from './direction';

class Ant {
  /**
   * Initialize Ant
   * @param startPoint {Node}
   * @param targetDirection {Link}
   */
  constructor(startPoint, targetDirection = startPoint.getRandomDirection()) {
    this.posX = startPoint.posX;
    this.posY = startPoint.posY;

    this.targetDirection = targetDirection;

    this.isForwardMode = true;

    this.initPath = () => {
      const newTarget = startPoint.getRandomDirection().target;
      const initialDir = new Direction(newTarget, startPoint, this.targetDirection.originLink);
      initialDir.backward = new Direction(startPoint, newTarget, this.targetDirection.originLink);
      this.path = [initialDir];
    };

    this.distanceTraveled = 0;
    this.backwardPath = [];

    this.initPath();
  }
}

export default Ant;
