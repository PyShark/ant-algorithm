import config from 'config';
import rotate from 'helpers/imageRotate';

export const ANTS_IMAGES = {};
ANTS_IMAGES.default = new Image();
ANTS_IMAGES.default.src = config.antImagePath;

/**
 * Add new ant image
 * @param angle in radians
 */
function addNewAntsImage(angle) {
  const degrees = Math.ceil(angle * (180 / Math.PI));

  if (!ANTS_IMAGES[degrees]) {
    ANTS_IMAGES[degrees] = rotate(ANTS_IMAGES.default, angle);
  }
}

class Direction {
  /**
   * Create one direction represent for node path
   * @param originNode
   * @param targetNode
   * @param link
   */
  constructor(originNode, targetNode, link) {
    this.origin = originNode;
    this.target = targetNode;

    this.dx = targetNode.posX - originNode.posX;
    this.dy = targetNode.posY - originNode.posY;

    this.angle = Math.atan2(this.dy, this.dx) + Math.PI / 2;

    addNewAntsImage(this.angle);
    this.antImage = ANTS_IMAGES[Math.ceil(this.angle * (180 / Math.PI))] || ANTS_IMAGES.default;

    this.originLink = link;
    this.backward = null;
  }
}


export default Direction;
