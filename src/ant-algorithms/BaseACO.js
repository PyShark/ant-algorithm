import config from 'config';
import { states as NODE_STATES } from 'entity/node';

import randomDistribute from 'helpers/randomDistribute';

class BaseACO {
  constructor() {
    this.pheromoneCount = 1;
    this.pheromoneEvaporation = 0.1;

    this.alpha = 1;
    this.beta = 0;

    this.antLinks = {};
    this.antVelocity = 1;
    this.updateLinksTimeout = null;

    this.showText = true;
  }

  setAlpha(alpha) { this.alpha = alpha; }

  setBeta(beta) { this.beta = beta; }

  setPheromoneCount(count) { this.pheromoneCount = count; }

  setPheromoneEvaporation(count) { this.pheromoneEvaporation = count; }

  updateLinksCycle() {
    Object.values(this.antLinks).forEach(l => { this.updateLinkText(l); });

    this.updateLinksTimeout = setTimeout(() => { this.updateLinksCycle(); }, config.evaporationTiming);
  }

  startUpdateLinks() {
    if (!this.updateLinksTimeout) {
      this.updateLinksCycle();
    }
  }

  pauseUpdateLinks() {
    clearTimeout(this.updateLinksTimeout);
    this.updateLinksTimeout = null;
  }

  routeTextVisibility(show) {
    this.showText = show;
  }

  /**
   * Add route to ACO routing object
   * @param link {Link}
   */
  addNewLink({ id, distance, context }) {
    this.antLinks[id] = {
      context,
      distance: Math.round(distance * 100) / 100,
      phDistance: Math.round(distance) / config.phDistanceCoeff,
      pheromone: 1,
    };
  }

  removeLink({ id }) {
    delete this.antLinks[id];
  }

  /**
   * Update text near link
   * @param text {Text}
   */
  addLinkText(text) {
    const link = this.antLinks[text.originElement.id];

    if (link) {
      this.antLinks[text.originElement.id].text = text;
      this.updateLinkText(link);
    }
  }

  updateLinkText(antLink, formatter = link => `${link.distance} / ${link.pheromone}`) {
    if (this.showText) {
      antLink.text.context.innerHTML = formatter(antLink);
    }

    // hue - 0, saturation in ph %, light in ph % between 45 and 90
    const saturation = (antLink.pheromone * 100) / config.hslCoeff;
    const light = 80 - saturation;
    antLink.context.style.stroke = `hsl(0, ${saturation}%, ${light < 45 ? 45 : light}%)`;
  }

  resetLines() {
    Object.values(this.antLinks).forEach(al => {
      al.pheromone = 1;
      this.updateLinkText(al);
    });
  }

  /**
   * Call it when some node reached.
   * Also checks current target is source(start) or food(end) node.
   */
  nodeReached(ant) {
    const currentNode = ant.targetDirection.target;
    const { originState } = currentNode;

    if (originState === NODE_STATES.startPos) {
      ant.isForwardMode = true;
      ant.path = [];

      if (ant.backwardPath.length > 0) {
        ant.backwardPath.forEach(link => {
          if (this.antLinks[link.id]) this.updatePheromone(link, ant.distanceTraveled / 100);
        });
      }

      ant.distanceTraveled = 0;
      ant.backwardPath = [];
    } else if (ant.isForwardMode) {
      ant.path.push(ant.targetDirection.backward);
      if (originState === NODE_STATES.endPos) {
        ant.isForwardMode = false;
        ant.path = this.normalizeAntPath(ant.path);
      }
    }

    if (!ant.isForwardMode) {
      const path = ant.path.pop();

      if (!path || !path.originLink || !this.antLinks[path.originLink.id]) {
        ant.path = [];
        ant.targetDirection = this.getNextDirectionForNode(
          currentNode, ant.path.length > 0 ? ant.targetDirection.origin : {},
        );
      } else {
        ant.targetDirection = path;
        const { originLink } = path;

        ant.backwardPath.push(originLink);
        ant.distanceTraveled += originLink.distance;

        this.updatePheromone(originLink);
      }
    } else {
      ant.targetDirection = this.getNextDirectionForNode(
        currentNode, ant.path.length > 0 ? ant.targetDirection.origin : {},
      );
    }
  }

  updatePheromone({ id }, additionalDistance = 0) {
    // TODO: this kind of evaporation is only for SACO.

    // but first, do evaporation on all links.
    Object.values(this.antLinks).forEach(l => {
      const newPh = l.pheromone * (1 - this.pheromoneEvaporation);
      l.pheromone = newPh > 1 ? Math.floor(newPh) : 1;
    });

    if (this.antLinks[id]) {
      this.antLinks[id].pheromone += this.calcPheromoneCount(additionalDistance || this.antLinks[id].phDistance);
      this.antLinks[id].pheromone = Math.round(this.antLinks[id].pheromone);
    }
  }

  /**
   * Main function to get next direction according to pheromone probability
   * @param node {Node}
   * @param originNode
   * @returns {Direction}
   */
  getNextDirectionForNode(node, originNode = {}) {
    if (node.linkedDirections.length === 1) {
      return node.linkedDirections[0];
    }

    const directions = [];
    for (let i = 0; i < node.linkedDirections.length; i += 1) {
      const ld = node.linkedDirections[i];
      if (ld.target.id !== originNode.id && this.antLinks[ld.originLink.id]) {
        directions.push(ld);
      }
    }

    const data = directions.reduce((acc, cur) => {
      acc.push(this.calculatePheromoneProbability(this.antLinks[cur.originLink.id]));
      return acc;
    }, []);

    // eslint-disable-next-line no-param-reassign
    const dataSum = data.reduce((acc, cur) => { acc += cur; return acc; }, 0);

    const probabilities = data.map(e => (e / dataSum) * 100);

    return directions[randomDistribute(probabilities)];
  }

  calculatePheromoneProbability({ pheromone, distance }) {
    return pheromone ** this.alpha * (1 / distance) ** this.beta;
  }

  calcPheromoneCount(distance) {
    const newPh = this.pheromoneCount / distance;
    return newPh > 1 ? newPh : 1;
  }

  /**
   * Remove loop from route graph.
   * @returns {Direction[][]}
   */
  normalizeAntPath(path) {
    const newPath = [...path];

    for (let i = 0; i < newPath.length; i += 1) {
      for (let j = newPath.length - 1; j > i; j -= 1) {
        if (newPath[i].origin.id === newPath[j].origin.id) {
          newPath.splice(i + 1, j - i);
          break;
        }
      }
    }
    return newPath;
  }
}


export default BaseACO;
