import BaseACO from 'ant-algorithms/BaseACO';
import GridElement from 'entity/base/GridElement';

class SACO extends BaseACO {
  constructor() {
    super();

    GridElement.listenElementCreated('line', link => { this.addNewLink(link); });
    GridElement.listenElementCreated('text', text => { this.addLinkText(text); });

    GridElement.listenElementDeleted('line', line => { this.removeLink(line); });
  }
}

export default SACO;
