
export default {
  SVGNamespace: 'http://www.w3.org/2000/svg',

  antImagePath: './img/ant.png',
  antVelocity: 50,

  nodesAcross: 30,
  nodesDown: 15,

  nodeSizeSmall: 7,
  nodeSizeMiddle: 8,
  nodeSizeBig: 9,
  nodeSizeExtraBig: 11,
  indentSize: 22,

  hslCoeff: 2000, // pheromone count that represents 100% of red contrast,
  phDistanceCoeff: 50, // line length coefficient that reduce pheromone spraying for distance

  evaporationTiming: 1000, // ms how fast evaporation function calls
  phEvaporationCoeff: 2, // coefficient that reduce pheromone evaporation

  antImageWidth: 8,
  antImageHeight: 8,
};
