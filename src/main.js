import Grid from 'controllers/grid';
import AntCanvas from 'controllers/antCanvas';
import Slider from 'controllers/slider';
import ContextMenu from 'controllers/contextMenu';

import downloadJSON from 'helpers/jsonDownload';
import uploadJSON from 'helpers/jsonUpload';
import { resetGenerator } from 'helpers/uniqueIdGenerator';

import config from './config';

class App {
  constructor() {
    this.setupGridDimensions(config);
    Grid.listenDimensionsChange(d => {
      this.setupGridDimensions(d);
      resetGenerator();
      this.mainView.changeDimension(this.gridWidth, this.gridHeight);
      this.antView.changeDimension(this.gridWidth, this.gridHeight);
      d.callback();
    });

    // svgGrid for view representation
    this.mainView = new Grid(document.getElementById('svg-grid'), this.gridWidth, this.gridHeight);

    this.antView = new AntCanvas(document.getElementById('ant-canvas'), this.gridWidth, this.gridHeight);

    this.canvasPanel = document.getElementById('grid');
    this.contextMenu = new ContextMenu('context-menu');
    this.canvasPanel.addEventListener('contextmenu', e => {
      const menuItems = this.mainView.getOptionalMenuObject(e.target);
      if (menuItems && menuItems.length > 0) {
        e.preventDefault();
        this.contextMenu.show(e.pageX, e.pageY, menuItems);
        this.canvasPanel.addEventListener('mousedown', () => { this.contextMenu.hide(); }, false);
      }
    }, false);

    this.initButtons();
    this.initToggles();
    this.initSliders();
  }

  /*
   * Calling from index.html.
   * Initialize
   */
  init() {
    this.mainView.setupGrid();
  }

  setupGridDimensions({ nodesAcross, nodesDown }) {
    // setup grid dimensions
    this.gridWidth = nodesAcross * config.nodeSizeSmall * 2 + (nodesAcross - 1) * config.indentSize
      + config.nodeSizeSmall * 2; // also with some padding
    this.gridHeight = nodesDown * config.nodeSizeSmall * 2 + (nodesDown - 1) * config.indentSize
      + config.nodeSizeSmall * 2; // also with some padding
  }

  initButtons() {
    this.handleClick('buttonStart', () => {
      this.antView.resetLineText();

      // hide circles
      this.mainView.primaryLayerVisibility(false);
      this.circlesCheckbox.checked = false;

      this.antView.pushAntsToRoute();
    });
    this.handleClick('buttonSaveGraph', () => downloadJSON(this.mainView.getGraphData(), 'savedGrid'));
    this.handleClick('buttonGraphUpload', () => document.getElementById('graphUpload').click());

    this.handleClick('buttonAntClear', () => {
      this.mainView.primaryLayerVisibility(true);
      this.circlesCheckbox.checked = true;

      this.antView.clearGrid();
      this.antView.resetLineText();
    });

    this.handleClick('buttonGraphClear', () => {
      this.mainView.clearGrid();
      this.antView.clearGrid();
    });

    document.getElementById('graphUpload')
      .addEventListener('change', event => uploadJSON(event.target.files)
        .then(data => this.mainView.loadPathNodesToGrid(data))
        .catch(console.error)
        .finally(() => { event.target.value = null; }),
      false);
  }

  handleClick(elId, handler) {
    const el = document.getElementById(elId);
    el.addEventListener('click', e => handler(e));
    return el;
  }

  initToggles() {
    this.handleClick('ant_layer_check', ({ target }) => {
      this.antView.antVisibilityChange(target.checked);
    });

    this.handleClick('route_info_check', ({ target }) => {
      this.mainView.routeInfoLayerVisibility(target.checked);
      this.antView.algorithm.routeTextVisibility(target.checked);
    });

    this.circlesCheckbox = this.handleClick('primary_circles_check', ({ target }) => {
      this.mainView.primaryLayerVisibility(target.checked);
    });
  }

  initSliders() {
    const sliders = [];

    sliders.push(
      new Slider(
        document.getElementById('ant_count'),
        document.getElementById('ant_count_bar'),
        document.getElementById('ant_count_value'),
        count => { this.antView.updateAntCount(count); },
        true,
      ),
      new Slider(
        document.getElementById('ant_speed'),
        document.getElementById('ant_speed_bar'),
        document.getElementById('ant_speed_value'),
        count => { this.antView.updateAntVelocity(count); },
        true,
      ),
      new Slider(
        document.getElementById('ph_count'),
        document.getElementById('ph_count_bar'),
        document.getElementById('ph_count_value'),
        count => { this.antView.algorithm.setPheromoneCount(count); },
        true,
      ),
      new Slider(
        document.getElementById('ph_evap'),
        document.getElementById('ph_evap_bar'),
        document.getElementById('ph_evap_value'),
        count => { this.antView.algorithm.setPheromoneEvaporation(count); },
        true,
      ),
      new Slider(
        document.getElementById('alpha'),
        document.getElementById('alpha_bar'),
        document.getElementById('alpha_value'),
        count => { this.antView.algorithm.setAlpha(count); },
        true,
      ),
      new Slider(
        document.getElementById('beta'),
        document.getElementById('beta_bar'),
        document.getElementById('beta_value'),
        count => { this.antView.algorithm.setBeta(count); },
        true,
      ),
    );

    return sliders;
  }
}


export default App;
