
class Slider {
  constructor(slider, sliderBar, sliderValue, updateValueHandler, force = false) {
    this.slider = slider;
    this.sliderBar = sliderBar;
    this.sliderValue = sliderValue;

    this.updateValue = updateValueHandler;
    this.isDown = false;

    if (force) {
      this.updateValue(this.slider.value);
    }

    this.attachEventListeners();
  }

  attachEventListeners() {
    this.slider.addEventListener('mousedown', () => { this.dragHandler(); });
    this.slider.addEventListener('mousemove', () => { this.dragOn(); });
    this.slider.addEventListener('mouseup', () => { this.dragHandler(); });
    this.slider.addEventListener('click', () => { this.rangeValueHandler(); });
  }

  dragHandler() {
    this.isDown = !this.isDown;
  }

  dragOn() {
    if (!this.isDown) return;
    this.rangeValueHandler();
  }

  rangeValueHandler() {
    this.sliderBar.style.setProperty('width', `${this.sliderBar.value}%`);
    this.sliderValue.innerHTML = `${this.slider.value}`;
    this.updateValue(this.slider.value);
  }
}

export default Slider;
