import config from 'config';

import Layer from 'controllers/layer';

import Node, { states as NODE_STATES } from 'entity/node';
import Link, { states as LINE_STATES } from 'entity/link';
import Text, { states as TEXT_STATES } from 'entity/text';

import EventEmitter from 'helpers/eventEmitter';

// TODO: mocked sample json data for testing purpose.
// eslint-disable-next-line import/no-unresolved
const sampleData = require('../../graph-examples/middle-grid.json');

const gridStartPointEmitter = new EventEmitter();
const gridDimensionEmitter = new EventEmitter();

class Grid {
  constructor(viewCtx, gridWidth, gridHeight) {
    this.multiClickMode = false;

    // setup grid dimensions
    this.gridWidth = gridWidth;
    this.gridHeight = gridHeight;

    this.nodesAcross = config.nodesAcross;
    this.nodesDown = config.nodesDown;

    this.viewCtx = viewCtx;
    this.layers = {}; // bind unique layer id to layer object

    // define svg layers.
    this.circleLayer = this.addNewLayer(NODE_STATES.primary.id); // primary circle layer
    this.linksLayer = this.addNewLayer(LINE_STATES.primary.id); // path lines layer
    this.pathLayer = this.addNewLayer(NODE_STATES.path.id); // path circle layer
    this.pathTextLayer = this.addNewLayer(TEXT_STATES.lineText.id); // path circle layer
    this.activeCircleLayer = this.addNewLayer(NODE_STATES.click.id); // active(clicked) layer
  }

  /**
   * Append layer based on unique id to grid layers
   * @param id
   * @returns {Layer}
   */
  addNewLayer(id) {
    const newLayer = new Layer(id, this.viewCtx, config.SVGNamespace);

    this.layers[newLayer.id] = newLayer;
    return newLayer;
  }

  static listenStartPointChange(handler) {
    gridStartPointEmitter.on('change', handler);
  }

  static listenDimensionsChange(handler) {
    gridDimensionEmitter.on('change', handler);
  }

  changeStartPoint(newPoint = null) {
    this.startPoint = newPoint;
    gridStartPointEmitter.emit('change', this.startPoint);
  }

  changeEndPoint(newPoint = null) {
    this.endPoint = newPoint;
  }

  /**
   * Function for setup grid.
   */
  setupGrid() {
    this.viewCtx.setAttribute('width', this.gridWidth);
    this.viewCtx.setAttribute('height', this.gridHeight);

    // draw grid in grid
    this.drawCircles();

    // draw start and end points
    this.drawInitialPoints();

    // TODO: mock data
    this.loadPathNodesToGrid(sampleData);
  }

  /**
  * Draw circles in grid context and remove old.
  * Count of circles depend on variables from config file
  */
  drawCircles() {
    const values = this.circleLayer.getValues();

    if (values && values.length > 0) {
      values.forEach(node => { this.circleLayer.removeContext(node, true); });
    }

    for (let x = 0; x < this.nodesAcross; x += 1) {
      for (let y = 0; y < this.nodesDown; y += 1) {
        // create and draw circle on svg layer
        const node = new Node(x, y, this.circleLayer.id);
        this.circleLayer
          .addCircleOnLayer(node)
          .addClickEvent(e => this.handleNodeClick(e));
      }
    }
  }

  /**
   * Draw start and end circles in grid context.
   */
  drawInitialPoints() {
    // Generate random indexes
    const getRandomIndex = () => Math.floor(Math.random() * this.circleLayer.getValues().length);
    const startPointIndex = getRandomIndex();
    let endPointIndex;
    do { endPointIndex = getRandomIndex(); } while (startPointIndex === endPointIndex);

    this.changeStartPoint(this.circleLayer.getValues()[startPointIndex]);
    this.mainToPathLayer(this.startPoint, NODE_STATES.startPos)();

    this.changeEndPoint(this.circleLayer.getValues()[endPointIndex]);
    this.mainToPathLayer(this.endPoint, NODE_STATES.endPos)();
  }

  /**
   * Handle node click
   * @param event
   */
  handleNodeClick(event) {
    const targetCircle = this.circleLayer.get(event.target.id);
    const updateDomTasks = []; // here we push delegation closures

    const clickedNodes = this.activeCircleLayer.getValues();

    // handle unClick
    if (this.activeCircleLayer.get(targetCircle.id)) {
      this.changeLayer(targetCircle, { fromLayer: this.activeCircleLayer });

      updateDomTasks.push(() => targetCircle.updateCtxFromState(targetCircle.originState, { useToggle: true }));
    // handle link between nodes
    } else if (clickedNodes.length > 0) {
      if (!this.multiClickMode) { // node-to-node path creation
        clickedNodes.push(targetCircle);

        this.nodesToPath(clickedNodes); // also draw a line between and save the path
        updateDomTasks.push(() => targetCircle.updateCtxFromState(NODE_STATES.click));
        updateDomTasks.push(...clickedNodes.map(e => this.updateNewPathNodeCtx(e), this));
      } // TODO: `else` multi-click mode and `link together`
    // handle first click
    } else {
      this.changeLayer(targetCircle, { toLayer: this.activeCircleLayer, forceAdd: true, forceRm: false });
      updateDomTasks.push(() => targetCircle.updateCtxFromState(NODE_STATES.click));
    }

    // delay toggle right after appendChild ends and DOM refreshed
    requestAnimationFrame(() => updateDomTasks.forEach(t => t()));
  }

  /**
   * Upload graph data for grid representation.
   */
  getGraphData() {
    return { nodeA: this.nodesAcross, nodeB: this.nodesDown, nodes: this.pathLayer.getSerializeData() };
  }

  /**
   * Load predefined nodes to grid
   */
  loadPathNodesToGrid(rawData) {
    const data = {};

    data.nodes = this.circleLayer.deserializeNodes(rawData.nodes);
    rawData.nodeA = rawData.nodeA || config.nodesAcross;
    rawData.nodeD = rawData.nodeD || config.nodesDown;

    const isExtendA = rawData.nodeA !== this.nodesAcross;
    const isExtendD = rawData.nodeD !== this.nodesDown;

    if (data.nodes.length > 0) {
      // remove all objects from grid
      this.clearGrid(true);
      if ((isExtendA || isExtendD)) {
        this.nodesAcross = rawData.nodeA;
        this.nodesDown = rawData.nodeD;

        gridDimensionEmitter.emit('change', {
          nodesAcross: rawData.nodeA,
          nodesDown: rawData.nodeD,
          callback: () => {
            this.updateUploadNodes(data);
          },
        });
      } else {
        this.updateUploadNodes(data);
      }
    }
  }

  updateUploadNodes(data) {
    const updateDomTasks = [];
    // setup new graph based on new nodes
    const nodesMapping = data.nodes.reduce((acc, rawNode) => {
      const node = this.circleLayer.get(rawNode.id);
      node.originState = rawNode.originState;

      if (node.isMainPoint()) {
        if (rawNode.originState.id === NODE_STATES.startPos.id) this.changeStartPoint(node);
        else this.changeEndPoint(node);

        updateDomTasks.push(this.mainToPathLayer(node, rawNode.originState));
      } else {
        updateDomTasks.push(this.updateNewPathNodeCtx(node, this.circleLayer));
      }
      acc[node.id] = node;
      return acc;
    }, {});

    // draw links between nodes
    data.nodes.forEach(node => {
      if (node.ln && node.ln.length > 0) {
        node.ln.forEach(n => this.nodesToPath([nodesMapping[node.id], nodesMapping[n]]));
        this.nodesToPath([]);
      }
    });

    // such delay makes animation smoother
    setTimeout(() => updateDomTasks.forEach(t => t()), 100);
  }

  /**
   * Construct route between all nodes.
   * Add to graph path linked nodes.
   * @param targetCircles{Node[]}
   */
  nodesToPath(targetCircles) {
    if (targetCircles.length > 0) {
      const nodes = [...targetCircles];
      while (nodes.length > 1) {
        const [leftNode] = nodes.splice(0, 1);

        // eslint-disable-next-line no-loop-func
        nodes
          .filter(n => !leftNode.isLinkedTo(n))
          .forEach(rightNode => {
            // links both node
            const link = new Link(leftNode, rightNode, this.linksLayer.id);
            const linkText = new Text((leftNode.posX + rightNode.posX) / 2, (leftNode.posY + rightNode.posY) / 2, link);

            link.setLinkText(linkText);

            this.pathTextLayer.addTextOnLayer(linkText);
            this.linksLayer.addLineOnLayer(link);
          });
      }
    }
  }

  /**
   * Move active node to path layer. Return closure for update state
   * @param node
   * @param fromLayer
   * @returns {function(): void}
   */
  updateNewPathNodeCtx(node, fromLayer = this.activeCircleLayer) {
    const isMainPoint = node.isMainPoint();
    const isOnLayer = fromLayer.get(node.id);

    this.changeLayer(node, {
      fromLayer: isOnLayer ? fromLayer : this.layers[node.originLayerId],
      toLayer: this.pathLayer,
      forceAdd: true,
      forceRm: isOnLayer && fromLayer.id === this.activeCircleLayer.id, // remove only from active layer
    });

    if (!isMainPoint) node.changeOriginState(NODE_STATES.path);

    return () => node.updateCtxFromState(NODE_STATES.path, {
      forceUpdateState: !isMainPoint, forceUpdateClass: !isMainPoint, removeClass: NODE_STATES.click.class,
    });
  }

  /**
   * Change or simple swap between grid layers
   * @param node
   * @param opts{{fromLayer?:{Layer}, toLayer?:{Layer}, forceRm?: boolean, forceAdd?: boolean}}
   */
  changeLayer(node, opts = {}) {
    const { fromLayer, toLayer, forceAdd, forceRm } = {
      fromLayer: this.layers[node.originLayerId],
      toLayer: this.layers[node.originLayerId],
      forceRm: true,
      forceAdd: false,
      ...opts,
    };

    fromLayer.removeContext(node, forceRm);
    toLayer.appendContext(node, forceAdd);
  }

  /**
   * Helps initialize main points: start and end.
   * Since end&start points doesn't have it's own layer,
   * you should use this function insteadof `updateNewPathNodeCtx`
   * @param node
   * @param toState
   */
  mainToPathLayer(node, toState) {
    this.changeLayer(node, { toLayer: this.pathLayer, forceRm: false, forceAdd: true });

    node.originLayerId = NODE_STATES.path.id;
    node.originState = toState;

    return () => node.updateCtxFromState(toState);
  }

  /**
   * Returns Nodes as a complete route for ant routing.
   * @returns {Node[]}
   */
  getRoute() {
    return this.pathLayer.getValues();
  }

  /**
   * Clear grid and leave main points only
   * @param fullClear - clear all
   */
  clearGrid(fullClear = false) {
    this.clearLineLayer(this.linksLayer);
    this.clearTextLayer(this.pathTextLayer);

    this.clearCircleLayer(this.pathLayer, fullClear);
    this.clearCircleLayer(this.activeCircleLayer, fullClear);
  }

  /**
   * Clear circles layer data - swap it to primary layer
   * @param layer
   * @param forceClearAll - clear even main points
   */
  clearCircleLayer(layer, forceClearAll = false) {
    const values = layer.getValues();

    if (values && values.length > 0) {
      values.forEach(node => {
        const isMain = node.isMainPoint() && !forceClearAll;
        if (!isMain) {
          this.changeLayer(node, { fromLayer: layer, toLayer: this.circleLayer, forceAdd: true });

          node.originLayerId = this.circleLayer.id;
          node.originState = NODE_STATES.primary;
        }

        // should be setTimeout insteadof raf since it could be fires earlier than node in new layer ready
        setTimeout(() => node.updateCtxFromState(
          isMain ? node.originState : NODE_STATES.primary,
          { forceUpdateState: true, forceUpdateClass: true, resetClass: NODE_STATES.primary.class },
        ));
      });
    }
  }

  clearTextLayer(layer) {
    layer.getValues().forEach(line => layer.removeContext(line), true);
    layer.clear();
  }

  changeDimension(newW, newH) {
    this.gridWidth = newW;
    this.gridHeight = newH;

    this.viewCtx.setAttribute('width', this.gridWidth);
    this.viewCtx.setAttribute('height', this.gridHeight);

    this.drawCircles();
  }

  /**
   * Return context menu depends on current DOM target]
   * @param target - event target
   * @returns {{ text: string, action: Function }[] | false}
   */
  getOptionalMenuObject(target) {
    let menu = [];

    switch (target.nodeName) {
      case 'circle': {
        const targetNode = this.circleLayer.get(target.id);
        const { originState } = targetNode;

        if ([NODE_STATES.startPos, NODE_STATES.endPos, NODE_STATES.path].some(s => originState === s)) {
          menu.push({ text: 'Delete node',
            action: () => {
              for (let i = 0; i < targetNode.linkedDirections.length; i += 1) {
                this.removeLineFromGrid(targetNode.linkedDirections[i].originLink);
                i -= 1;
              }

              this.removeNodeFromGrid(targetNode);
              targetNode.removeItem();
            } });
          if (originState !== NODE_STATES.startPos) {
            menu.push({ text: 'Change to start point', action: () => { this.swapToNewStartPoint(targetNode); } });
          }
          if (originState !== NODE_STATES.endPos) {
            menu.push({ text: 'Change to end point', action: () => { this.swapToNewEndPoint(targetNode); } });
          }
        }
        break;
      }
      case 'line': {
        menu.push({
          text: 'Delete line',
          action: () => {
            const targetLink = this.linksLayer.get(target.id);
            this.removeLineFromGrid(targetLink);
          },
        });
        break;
      }
      default: {
        menu = false;
      }
    }

    return menu;
  }

  /**
   * Remove line from grid
   * @param link {Link}
   */
  removeLineFromGrid(link) {
    this.linksLayer.removeContext(link, true);
    this.pathTextLayer.removeContext(link.text, true);
    this.removeAllBindLinks(link);
    link.removeItem();
  }

  /**
   * Remove node from grid
   * @param node {Node}
   */
  removeNodeFromGrid(node) {
    node.originState = NODE_STATES.primary;

    if (this.startPoint && this.startPoint.id === node.id) this.changeStartPoint();
    if (this.endPoint && this.endPoint.id === node.id) this.changeEndPoint();

    this.changeLayer(node,
      {
        fromLayer: this.activeCircleLayer.get(node.id) ? this.activeCircleLayer : this.layers[node.originLayerId],
        toLayer: this.circleLayer,
        forceAdd: true,
      });

    setTimeout(() => {
      node.updateCtxFromState(NODE_STATES.primary,
        { forceUpdateState: true, forceUpdateClass: true, resetClass: NODE_STATES.primary.class });
    });
  }

  swapToNewStartPoint(targetNode) {
    // eslint-disable-next-line no-multi-assign
    targetNode.originState = NODE_STATES.startPos;

    if (this.startPoint) this.startPoint.originState = NODE_STATES.path;

    setTimeout(() => {
      targetNode.updateCtxFromState(NODE_STATES.startPos,
        { forceUpdateState: true, forceUpdateClass: true, resetClass: NODE_STATES.primary.class });

      if (this.startPoint) {
        this.startPoint.updateCtxFromState(NODE_STATES.path,
          { forceUpdateState: true, forceUpdateClass: true, resetClass: NODE_STATES.primary.class });
      }

      this.changeStartPoint(targetNode);
      if (this.startPoint === this.endPoint) {
        this.changeEndPoint();
      }
    });
  }

  swapToNewEndPoint(targetNode) {
    // eslint-disable-next-line no-multi-assign
    targetNode.originState = NODE_STATES.endPos;

    if (this.endPoint) this.endPoint.originState = NODE_STATES.path;

    setTimeout(() => {
      targetNode.updateCtxFromState(NODE_STATES.endPos,
        { forceUpdateState: true, forceUpdateClass: true, resetClass: NODE_STATES.primary.class });

      if (this.endPoint) {
        this.endPoint.updateCtxFromState(NODE_STATES.path,
          { forceUpdateState: true, forceUpdateClass: true, resetClass: NODE_STATES.primary.class });
      }

      this.changeEndPoint(targetNode);
      if (this.startPoint === this.endPoint) {
        this.changeStartPoint();
      }
    });
  }

  /**
   * Clear line layer data
   * @param layer
   */
  clearLineLayer(layer) {
    const pathNodes = this.pathLayer.getValues();
    if (pathNodes.length > 0) {
      pathNodes.forEach(node => { node.linkedDirections = []; });
    }

    layer.getValues().forEach(line => layer.removeContext(line), true);
    layer.clear();
  }

  routeInfoLayerVisibility(show) {
    this.pathTextLayer.changeVisibility(show);
  }

  primaryLayerVisibility(show) {
    this.circleLayer.changeVisibility(show);
  }

  /**
   * Remove all references to current link
   * @param targetLink {Link}
   */
  removeAllBindLinks(targetLink) {
    const nodeA = this.circleLayer.get([targetLink.startPos.id]);

    nodeA.linkedDirections.splice(
      nodeA.linkedDirections.findIndex(ld => ld.originLink.id === targetLink.id), 1,
    );

    const nodeB = this.circleLayer.get([targetLink.endPos.id]);
    nodeB.linkedDirections.splice(
      nodeB.linkedDirections.findIndex(ld => ld.originLink.id === targetLink.id), 1,
    );
  }
}

export default Grid;
