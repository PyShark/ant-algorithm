import Node, { states as NODE_STATES } from 'entity/node';
import { states as LINE_STATES } from 'entity/link';
import { states as TEXT_STATES } from 'entity/text';

class Layer {
  constructor(id, ctx, namespace = null) {
    this.id = id;
    this.ns = namespace;
    this.data = {};

    this.context = ctx.appendChild(document.createElementNS(this.ns, 'g'));
  }

  changeVisibility(isShow) {
    if (isShow) {
      this.context.classList.remove('layer_hide');
    } else {
      this.context.classList.add('layer_hide');
    }
  }

  /**
   * Remove child from context
   * @param elem {GridElement}
   * @param forceAdd
   */
  appendContext(elem, forceAdd = false) {
    if (forceAdd) this.add(elem);
    this.context.appendChild(elem.context);
  }

  /**
   * Remove child from context
   * @param elem {GridElement}
   * @param forceRm
   */
  removeContext({ context, id }, forceRm = false) {
    if (forceRm) this.rm(id);
    this.context.removeChild(context);
  }

  /**
   * Return layer data in array representation
   * @returns {GridElement[]}
   */
  getValues() {
    return Object.values(this.data);
  }

  /**
   * Return layer element by id
   * @returns {GridElement}
   */
  get(id) {
    return this.data[id];
  }

  add(...elements) {
    elements.forEach(e => {
      if (!this.get(e.id)) {
        this.data[e.id] = e;
      }
    });
  }

  rm(id) {
    delete this.data[id];
  }

  /**
   * Reset all layer data to initial state
   */
  clear() {
    this.data = {};
  }

  /**
   * Return path data objects for future serialization
   * @returns {*}
   */
  getSerializeData() {
    return this.getValues().map(e => e.getSerializeElement());
  }

  /**
   * Deserialize node to layer
   */
  deserializeNodes(rawNodes) {
    let data = [];
    if (Array.isArray(rawNodes)) {
      data = rawNodes.map(el => Node.deserializeElement(el));
    } else throw new Error('File corrupted');

    return data;
  }

  /**
   * Set node params, fill in ctx, append to layer, set context
   * @param node{Node}
   * @returns {Node}
   */
  addCircleOnLayer(node) {
    node.context.setAttributeNS(null, 'cx', node.posX);
    node.context.setAttributeNS(null, 'cy', node.posY);
    node.context.setAttributeNS(null, 'id', node.id);

    node.updateCtxFromState(NODE_STATES.primary); // force update ctx state to primary

    this.appendContext(node, true);
    return node;
  }

  /**
   * Set node params, fill in ctx, append to layer, set context
   * @param text{Text}
   * @returns {Text}
   */
  addTextOnLayer(text) {
    text.context.setAttributeNS(null, 'x', text.posX);
    text.context.setAttributeNS(null, 'y', text.posY);
    text.context.setAttributeNS(null, 'id', text.id);

    text.updateCtxFromState(TEXT_STATES.lineText); // force update ctx state to primary

    this.appendContext(text, true);
    return text;
  }

  /**
   * Set line params, fill in ctx, append to layer
   * @param link{Link}
   * @returns {Link}
   */
  addLineOnLayer(link) {
    link.context.setAttributeNS(null, 'x1', link.startPos.x);
    link.context.setAttributeNS(null, 'x2', link.endPos.x);
    link.context.setAttributeNS(null, 'y1', link.startPos.y);
    link.context.setAttributeNS(null, 'y2', link.endPos.y);
    link.context.setAttributeNS(null, 'id', link.id);

    this.appendContext(link, true);

    // svg line transition with calculated length
    const length = link.context.getTotalLength();
    // Set up the starting positions
    link.context.style.strokeDasharray = `${length} ${length}`;
    link.context.style.strokeDashoffset = length;
    // Trigger a layout so styles are calculated & the browser
    // picks up the starting position before animating
    link.context.getBoundingClientRect();
    link.context.style.strokeDashoffset = '0';

    link.updateCtxFromState(LINE_STATES.primary); // force update ctx state to primary
    return link;
  }
}

export default Layer;
