import config from 'config';
import SACO from 'ant-algorithms/S-ACO';

import Grid from 'controllers/grid';

import Ant from 'entity/ant';

import { getDistanceBetween, calculateVectorMagnitude } from 'helpers/drawCalculations';

class AntCanvas {
  constructor(canvas, gridWidth, gridHeight) {
    this.gridWidth = gridWidth;
    this.gridHeight = gridHeight;

    this.antCount = 1;
    this.antCanvas = canvas;
    this.antCanvasCtx = canvas.getContext('2d');
    this.antCanvas.width = gridWidth;
    this.antCanvas.height = gridHeight;

    this.antVelocity = config.antVelocity / 100;
    this.antsMoveFrame = null;
    this.showAnts = true;

    this.algorithm = new SACO();
    this.algorithm.antVelocity = this.antVelocity;

    Grid.listenStartPointChange(newPoint => {
      this.startPoint = newPoint;
    });
  }

  clearGrid() {
    if (this.antsMoveFrame) {
      cancelAnimationFrame(this.antsMoveFrame);
      this.algorithm.pauseUpdateLinks();
    }
    this.clearAntLayer();
  }

  clearAntLayer() {
    this.antCanvasCtx.clearRect(0, 0, this.gridWidth, this.gridHeight);
  }

  antVisibilityChange(show) {
    this.showAnts = show;
  }

  pauseAnts() {
    cancelAnimationFrame(this.antsMoveFrame);
    this.algorithm.pauseUpdateLinks();
  }

  resumeAnts() {
    if (this.antsMoveFrame) {
      cancelAnimationFrame(this.antsMoveFrame);
    }

    this.animateRoutePath();
    this.algorithm.startUpdateLinks();
  }

  /**
   * Push Ants to route
   */
  pushAntsToRoute() {
    if (!this.startPoint) {
      console.error('You can`t start ant algorithm without start point. Choose it by using right click to path node.');
      return;
    }

    if (this.startPoint.linkedDirections.length === 0) {
      console.error('You can`t start ant algorithm without route which links to start node.');
      return;
    }

    this.antsGroup = [];

    for (let i = 0; i < this.antCount; i += 1) {
      this.antsGroup.push(new Ant(this.startPoint));
    }

    if (this.antsMoveFrame) {
      cancelAnimationFrame(this.antsMoveFrame);
    }

    this.algorithm.startUpdateLinks();
    this.animateRoutePath();
  }

  /**
   * Set ant count
   * @param count
   */
  updateAntCount(count) {
    this.antCount = count || 1;
  }

  resetLineText() {
    this.algorithm.resetLines();
  }

  changeDimension(newW, newH) {
    this.gridWidth = newW;
    this.gridHeight = newH;

    this.antCanvas.width = this.gridWidth;
    this.antCanvas.height = this.gridHeight;
  }


  /**
   * Set ant velocity percents
   * @param velocityPercents
   */
  updateAntVelocity(velocityPercents) {
    this.antVelocity = (velocityPercents / 100) * config.antVelocity;
    this.algorithm.antVelocity = this.antVelocity;
  }

  /**
   * Base function for each frame ant route path
   */
  animateRoutePath() {
    this.clearAntLayer();

    for (let i = 0; i < this.antsGroup.length; i += 1) {
      const ant = this.antsGroup[i];
      let direction = ant.targetDirection;

      // if direction is disappear(probably target node deleted while ant move to it) - teleport it to start
      if (!direction && this.startPoint) {
        ant.posX = this.startPoint.posX;
        ant.posY = this.startPoint.posY;

        // eslint-disable-next-line no-multi-assign
        direction = ant.targetDirection = this.startPoint.getRandomDirection();
      }

      if (!direction || !this.startPoint) {
        this.antsGroup.splice(i, 1);
        // eslint-disable-next-line no-continue
        continue;
      }

      const distance = getDistanceBetween(
        ant.posX, ant.posY,
        direction.target.posX, direction.target.posY,
      );

      if (this.showAnts) this.drawAnt(ant.posX, ant.posY, direction.antImage);

      const { x, y } = calculateVectorMagnitude(ant, direction.target, this.antVelocity);

      if (distance < this.antVelocity) { // target node reached his destination node
        // Ants move straightforward only
        // ant.posX = direction.target.posX;
        // ant.posY = direction.target.posY;

        this.algorithm.nodeReached(ant);
      } else {
        ant.posX += x;
        ant.posY += y;
      }
    }

    // keep moving to target nodes
    this.antsMoveFrame = requestAnimationFrame(() => this.animateRoutePath());
  }

  /**
   * Draw Ant image on canvas.
   * Notice: use direction to fetch pre-defined ant image to prevent translate/rotations
   * Also notice: we not draw image, but draw canvas with image instead for performance reasons.
   * @param posX
   * @param posY
   * @param antImage
   */
  drawAnt(posX, posY, antImage) {
    this.antCanvasCtx.drawImage(antImage, posX - antImage.width / 2, posY - antImage.height / 2);
  }
}

export default AntCanvas;
