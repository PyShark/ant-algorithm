
class ContextMenu {
  constructor(ulElementId) {
    this.context = document.getElementById(ulElementId);
  }

  createNewItem(text, action) {
    const li = document.createElement('li');

    li.classList.add('menu-item');

    const btn = document.createElement('button');
    btn.classList.add('menu-btn');
    btn.addEventListener('click', e => {
      action(e);
      this.hide();
    });

    const span = document.createElement('span');
    span.innerHTML = text;
    span.classList.add('menu-text');
    btn.appendChild(span);

    li.appendChild(btn);

    this.context.appendChild(li);
  }

  hide() {
    this.context.classList.remove('menu-show');
  }

  /**
   * Dynamically append elements to context menu
   * @param x
   * @param y
   * @param menuItems {{ text: string, action: Function }[]}
   */
  show(x, y, menuItems) {
    while (this.context.firstChild) {
      this.context.removeChild(this.context.firstChild);
    }

    menuItems.forEach(mI => { this.createNewItem(mI.text, mI.action); });

    this.context.style.left = `${x}px`;
    this.context.style.top = `${y}px`;
    this.context.classList.add('menu-show');
  }
}

export default ContextMenu;
