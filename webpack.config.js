module.exports = {
  entry: './src/main.js',
  output: {
    path: `${__dirname}/dist/`,
    filename: 'bundle.js',
    libraryTarget: 'umd',
    library: 'MainApp',
    libraryExport: 'default',
  },
  resolve: { modules: ['src'], extensions: ['.js'] },
};
